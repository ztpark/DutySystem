package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程信息  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/zfm/bac-course")
public class BacCourseController {

}
