package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsRole;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * cms_role  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface CmsRoleMapper extends BaseMapper<CmsRole> {

}
