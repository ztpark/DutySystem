package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 实训室 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacTrainingRoom extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 实训室名称
     */
    private String roomName;

    /**
     * 实训中心 关联实训中心表的实训中心id
     */
    private Long trainingcenterId;

    /**
     * 服务专业
     */
    private String professionalNumber;

    /**
     * 开设课程
     */
    private String courses;

    /**
     * 具体地点
     */
    private String location;

    /**
     * 人数容量
     */
    private Integer peopleNumber;

    /**
     * 管理责任人编号
     */
    private String responsibleNumber;

}
