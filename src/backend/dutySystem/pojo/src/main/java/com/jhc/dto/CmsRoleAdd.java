package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/13 15:22
 */
@Data
public class CmsRoleAdd implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 角色中文名称 角色中文名称
     */
    @NotBlank(message = "角色名称不能为空")
    private String name;

    /**
     * 权限码 权限编码
     */
    @NotNull(message = "权限码不能为空")
    private List<String> authDataList;
}
