package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BizNoticeObject;
import com.jhc.mapper.BizNoticeObjectMapper;
import com.jhc.service.IBizNoticeObjectService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务消息对象表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BizNoticeObjectServiceImpl extends ServiceImpl<BizNoticeObjectMapper, BizNoticeObject> implements IBizNoticeObjectService {

}
