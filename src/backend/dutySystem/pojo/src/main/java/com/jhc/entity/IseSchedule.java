package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 值班计划  
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IseSchedule extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学期id
     */
    private Long termId;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 值班领导id
     */
    private String leaderNumber;

    /**
     * 值班中层id
     */
    private String middleNumber;

    /**
     * 值班班主任id
     */
    private String dutyOneNumber;

    /**
     * 值班班主任2id
     */
    private String dutyTwoNumber;

    /**
     * 值班辅导员
     */
    private String instructorNumber;

    /**
     * 创建人id
     */
    private String createrNumber;

    /**
     * 计划
     */
    private String plan;

    /**
     * 总结
     */
    private String summary;


}
