package com.jhc.utils;

import cn.hutool.core.util.StrUtil;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;
import org.springframework.beans.BeanUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/13 16:00
 */
public class JHC {

    /**
     * 去头去尾
     *
     * @param content   字符串
     * @param startChar 开头字符串
     * @param endChar   结束字符串
     * @return 去掉后的字符串
     */
    public static String removeStartEnd(String content, String startChar, String endChar) {
        if (content == null) {
            return null;
        }
        if (startChar == null) {
            startChar = "";
        }
        if (endChar == null) {
            endChar = "";
        }
        return StrUtil.removePrefix(StrUtil.removeSuffix(content, endChar), startChar);
    }

    /**
     * 获取IP地址
     *
     * @param request 请求服务
     * @return IP地址
     */
    public static String getIpAddr(HttpServletRequest request) {
        String ipAddress = null;
        try {
            ipAddress = request.getHeader("x-forwarded-for");
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getHeader("WL-Proxy-Client-IP");
            }
            if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
                ipAddress = request.getRemoteAddr();
                if ("127.0.0.1".equals(ipAddress)) {
                    // 根据网卡取本机配置的IP
                    InetAddress inet = null;
                    try {
                        inet = InetAddress.getLocalHost();
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                    ipAddress = inet.getHostAddress();
                }
            }
            // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
            // "***.***.***.***".length()
            if (ipAddress != null && ipAddress.length() > 15) {
                // = 15
                if (ipAddress.indexOf(",") > 0) {
                    ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
                }
            }
        } catch (Exception e) {
            ipAddress = "";
        }
        return ipAddress;
    }


    /**
     * 树型结构
     * @param cmsPermissionList 权限列表
     * @return 树型权限结构
     */
    public static List<CmsTreePermission> cmsTreePermissions(List<CmsPermission> cmsPermissionList) {
        List<CmsTreePermission> cmsTreePermissionList = new ArrayList<>();
        List<CmsTreePermission> originData = new ArrayList<>();
        for (CmsPermission cmsPermission : cmsPermissionList) {
            CmsTreePermission cmsTreePermission = new CmsTreePermission();
            BeanUtils.copyProperties(cmsPermission, cmsTreePermission);
            originData.add(cmsTreePermission);
        }

        for (int i = 0; i < originData.size(); i++) {
            if (originData.get(i).getParentId() == 0) {
                cmsTreePermissionList.add(originData.get(i));
                continue;
            }
            for (CmsTreePermission originDatum : originData) {
                if (originData.get(i).getParentId().equals(originDatum.getId())) {
                    if (originDatum.getChildrenList() == null) {
                        originDatum.setChildrenList(new ArrayList<>());
                    }
                    originDatum.getChildrenList().add(originData.get(i));
                }
            }
        }
        return cmsTreePermissionList;
    }
}
