package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacMajor;
import com.jhc.mapper.BacMajorMapper;
import com.jhc.service.IBacMajorService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专业档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacMajorServiceImpl extends ServiceImpl<BacMajorMapper, BacMajor> implements IBacMajorService {

}
