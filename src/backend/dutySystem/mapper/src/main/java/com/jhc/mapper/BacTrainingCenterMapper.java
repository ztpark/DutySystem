package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacTrainingCenter;

/**
 * <p>
 * 实训中心   Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTrainingCenterMapper extends BaseMapper<BacTrainingCenter> {

}
