package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 行政班 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacClass extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 专业编号
     */
    private String majorNumber;

    /**
     * 学院编号
     */
    private String collegeNumber;

    /**
     * 班主任id
     */
    private String teacherNumber;

    /**
     * 是否启用
     */
    private Boolean isEnabled;


}
