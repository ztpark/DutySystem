package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.IseInspection;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IIseInspectionService extends IService<IseInspection> {

}
