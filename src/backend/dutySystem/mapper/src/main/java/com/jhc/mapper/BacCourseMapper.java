package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacCourse;

/**
 * <p>
 * 课程信息  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacCourseMapper extends BaseMapper<BacCourse> {

}
