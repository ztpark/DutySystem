package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacStuTeach;

/**
 * <p>
 * 学生教学班关系表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacStuTeachMapper extends BaseMapper<BacStuTeach> {

}
