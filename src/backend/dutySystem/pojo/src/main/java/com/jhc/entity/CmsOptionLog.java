package com.jhc.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 访问日志信息 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CmsOptionLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 业务名称
     */
    private String title;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 请求IP
     */
    private String address;

    /**
     * 请求资源路径
     */
    private String url;

    /**
     * 请求方式
     */
    private String reqMethod;

    /**
     * 请求参数
     */
    private String reqParam;

    /**
     * 返回参数
     */
    private String resParam;

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 耗时
     */
    private String timeout;

    /**
     * 用户唯一编号
     */
    private Long userId;

    /**
     * 用户姓名
     */
    private String username;


}
