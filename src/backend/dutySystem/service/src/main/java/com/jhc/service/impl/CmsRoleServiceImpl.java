package com.jhc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.CmsRoleUpdate;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;
import com.jhc.entity.CmsRole;
import com.jhc.entity.CmsTeacher;
import com.jhc.entity.UserAuthData;
import com.jhc.mapper.CmsPermissionMapper;
import com.jhc.mapper.CmsRoleMapper;
import com.jhc.mapper.CmsTeacherMapper;
import com.jhc.service.ICmsRoleService;
import com.jhc.service.RedisService;
import com.jhc.utils.JHC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * cms_role  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class CmsRoleServiceImpl extends ServiceImpl<CmsRoleMapper, CmsRole> implements ICmsRoleService {

    @Autowired
    private CmsTeacherMapper cmsTeacherMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private CmsRoleMapper cmsRoleMapper;

    @Autowired
    private CmsPermissionMapper cmsPermissionMapper;

    @Override
    @Async("threadPool")
    public void updateRole(CmsRoleUpdate cmsRoleUpdate) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<CmsTeacher> cmsAdminList = cmsTeacherMapper.selectList(new QueryWrapper<CmsTeacher>()
                .lambda()
                .select(CmsTeacher::getNumber, CmsTeacher::getAuthData));
        for (CmsTeacher cmsAdmin : cmsAdminList) {
            UserAuthData userAuthData = new UserAuthData();
            userAuthData = JSON.parseObject(cmsAdmin.getAuthData(), UserAuthData.class);
            if (userAuthData.getRoleList() != null) {
                System.out.println(userAuthData.getRoleList());
                if (userAuthData.getRoleList().contains(cmsRoleUpdate.getId() + "")) {
                    String staffNumber = cmsAdmin.getNumber();
                    if (cmsRoleUpdate.getIfFast()) {
                        redisService.remove(staffNumber);
                    } else {
                        redisService.expire(staffNumber, 60 * 60);
                    }
                }
            }
        }
        log.error("线程结束");
    }

    @Override
    public List<CmsTreePermission> getPermissionList(Long roleId) {
        CmsRole cmsRole = cmsRoleMapper.selectOne(new QueryWrapper<CmsRole>().lambda().select(CmsRole::getAuthData)
                .eq(CmsRole::getId, roleId));
        if (cmsRole == null) {
            return null;
        }
        String authString = cmsRole.getAuthData();
        UserAuthData userAuthData = JSON.parseObject(authString, UserAuthData.class);
        List<String> permissionList = userAuthData.getAuthList();
        if (permissionList == null || permissionList.size() == 0) {
            return null;
        }
        List<CmsPermission> cmsPermissionList = cmsPermissionMapper.selectList(
                new QueryWrapper<CmsPermission>().lambda().select(CmsPermission::getId,CmsPermission::getParentId,
                        CmsPermission::getPermissionName, CmsPermission::getValue)
                        .in(CmsPermission::getValue, permissionList)
        );

        return JHC.cmsTreePermissions(cmsPermissionList);
    }
}
