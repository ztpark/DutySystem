package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacCollege;

/**
 * <p>
 * 院系档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacCollegeMapper extends BaseMapper<BacCollege> {

}
