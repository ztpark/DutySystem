package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsOptionLog;

/**
 * <p>
 * 访问日志信息  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface CmsOptionLogMapper extends BaseMapper<CmsOptionLog> {

}
