package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsTeacher;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 教师档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface CmsTeacherMapper extends BaseMapper<CmsTeacher> {

}
