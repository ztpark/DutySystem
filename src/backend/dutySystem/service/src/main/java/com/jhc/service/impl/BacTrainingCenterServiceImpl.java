package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacTrainingCenter;
import com.jhc.mapper.BacTrainingCenterMapper;
import com.jhc.service.IBacTrainingCenterService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 实训中心   服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTrainingCenterServiceImpl extends ServiceImpl<BacTrainingCenterMapper, BacTrainingCenter> implements IBacTrainingCenterService {

}
