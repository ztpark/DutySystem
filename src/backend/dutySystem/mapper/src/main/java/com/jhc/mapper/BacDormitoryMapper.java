package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacDormitory;

/**
 * <p>
 * 寝室   Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacDormitoryMapper extends BaseMapper<BacDormitory> {

}
