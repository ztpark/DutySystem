package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 院系档案 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacCollege extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 部门编号
     */
    private String number;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 隶属部门
     */
    private String parentNumber;

    /**
     * 显示顺序
     */
    private Integer sort;


}
