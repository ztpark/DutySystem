package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 业务消息表 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BizNotice extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 通知对象类型
     */
    private Integer objectType;

    /**
     * 管理员id
     */
    private String adminNumber;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;


}
