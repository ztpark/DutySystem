package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacCollege;

/**
 * <p>
 * 院系档案  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacCollegeService extends IService<BacCollege> {

}
