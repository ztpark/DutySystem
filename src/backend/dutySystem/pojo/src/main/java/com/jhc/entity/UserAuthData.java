package com.jhc.entity;

import lombok.Data;

import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/14 15:26
 */
@Data
public class UserAuthData {
    private List<String> authList;
    private List<String> roleList;
}
