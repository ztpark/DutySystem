package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacTerm;

/**
 * <p>
 * 学期信息  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTermMapper extends BaseMapper<BacTerm> {

}
