package com.jhc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;
import com.jhc.mapper.CmsPermissionMapper;
import com.jhc.service.ICmsPermissionService;
import com.jhc.utils.JHC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * cms_permission  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class CmsPermissionServiceImpl extends ServiceImpl<CmsPermissionMapper, CmsPermission> implements ICmsPermissionService {

    @Autowired
    private CmsPermissionMapper cmsPermissionMapper;

    /**
     * 恢复权限使用
     *
     * @param permissionId 权限ID
     * @return 是否恢复
     */
    @Override
    public Boolean restart(List<Long> permissionId) {
        return cmsPermissionMapper.restart(permissionId);
    }

    /**
     * 禁用权限使用
     *
     * @param permissionId 权限ID
     * @return 是否禁用
     */
    @Override
    public Boolean ban(List<Long> permissionId) {
        return cmsPermissionMapper.ban(permissionId);
    }

    @Override
    public List<CmsTreePermission> tree() {
        List<CmsPermission> cmsPermissionList = cmsPermissionMapper.selectList(new QueryWrapper<CmsPermission>().lambda()
                .select(CmsPermission::getId, CmsPermission::getValue,CmsPermission::getParentId, CmsPermission::getPermissionName));
        return JHC.cmsTreePermissions(cmsPermissionList);
    }
}
