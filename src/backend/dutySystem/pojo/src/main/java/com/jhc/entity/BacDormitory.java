package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 寝室  
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacDormitory extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 房间号
     */
    private Integer room;

    /**
     * 楼幢
     */
    private String buildings;

    /**
     * 所属班级 可有多个班级用逗号隔开
     */
    private String classId;

    /**
     * 班主任 可有多个班主任用逗号隔开
     */
    private String classTeacherNumber;

    /**
     * 辅导员
     */
    private String instructorNumber;


}
