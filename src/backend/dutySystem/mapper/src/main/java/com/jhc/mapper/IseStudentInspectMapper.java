package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.IseStudentInspect;

/**
 * <p>
 * 学生检查表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IseStudentInspectMapper extends BaseMapper<IseStudentInspect> {

}
