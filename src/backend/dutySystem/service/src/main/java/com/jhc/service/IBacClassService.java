package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacClass;

/**
 * <p>
 * 行政班  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacClassService extends IService<BacClass> {

}
