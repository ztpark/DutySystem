package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 学生教学班关系表 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacStuTeach extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学生编号
     */
    private String studentInumber;

    /**
     * 教学班编号
     */
    private Long classId;


}
