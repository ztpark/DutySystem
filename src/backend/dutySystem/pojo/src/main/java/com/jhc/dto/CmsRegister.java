package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: zfm
 * @Date: 2019/11/13 10:37
 */
@Data
public class CmsRegister {
    /**
     * 职工号 职工号
     */
    @NotBlank(message = "职工号不能为空")
    private String number;

    /**
     * 密码 密码，加密
     */
    @NotBlank(message = "用户名不能为空")
    private String password;
}
