package com.jhc.controller;


import com.jhc.dto.CmsLogin;
import com.jhc.dto.CmsRegister;
import com.jhc.dto.CmsUserPermissionAdd;
import com.jhc.dto.CmsUserRoleAdd;
import com.jhc.entity.RedisUserInfo;
import com.jhc.service.ICmsTeacherService;
import com.jhc.service.RedisService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

/**
 * <p>
 * 教师档案  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/teacher")
public class CmsTeacherController {

    @Autowired
    private ICmsTeacherService iCmsTeacherService;
    @Autowired
    private RedisService redisService;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @PostMapping("/register")
    @ApiOperation("用户注册")
    public CommonResult register(@RequestBody @Validated CmsRegister cmsRegister) {
        if (iCmsTeacherService.register(cmsRegister)) {
            return CommonResult.success("注册成功");
        }
        return CommonResult.failed("工号已存在");
    }

    @PostMapping("/login")
    @ApiOperation("用户登陆")
    public CommonResult login(@RequestBody @Validated CmsLogin cmsLogin) {
        RedisUserInfo redisUserInfo = iCmsTeacherService.login(cmsLogin.getStaffNumber(), cmsLogin.getPassword());
        if (redisUserInfo.getToken() == null) {
            return CommonResult.validateFailed("账号密码错误");
        }
        redisUserInfo.setPermissionList(null);
        return CommonResult.success(redisUserInfo);
    }

    @ApiOperation("用户登出")
    @DeleteMapping("/logout/{staffNumber}")
    public CommonResult logout(@PathVariable String staffNumber) {
        System.out.println("参数:" + staffNumber);
        redisService.remove(staffNumber);
        return CommonResult.success("成功");
    }

    @ApiOperation("查询用户权限")
    @GetMapping("/{staffNumber}")
    @PreAuthorize("hasAuthority('user:permission:get')")
    public CommonResult getPermission(@PathVariable String staffNumber) {
        return CommonResult.success(iCmsTeacherService.getPermissionList(staffNumber), "查询成功");
    }

    @ApiOperation("更新用户权限")
    @PutMapping("/add/permission")
//    @PreAuthorize("hasAuthority('user:permission:add')")
    public CommonResult addPermission(@Validated @RequestBody CmsUserPermissionAdd cmsUserPermissionAdd) {
        // 去重
        cmsUserPermissionAdd.setAuthList(cmsUserPermissionAdd.getAuthList().stream().distinct().collect(Collectors.toList()));
        if (iCmsTeacherService.userAddPermission(cmsUserPermissionAdd)) {
            return CommonResult.success("添加成功");
        }
        return CommonResult.success("添加失败");
    }

    @ApiOperation("更新用户角色")
    @PutMapping("/add/role")
//    @PreAuthorize("hasAuthority('user:role:add')")
    public CommonResult addRole(@Validated @RequestBody CmsUserRoleAdd cmsUserRoleAdd) {
        // 去重
        cmsUserRoleAdd.setRoleList(cmsUserRoleAdd.getRoleList().stream().distinct().collect(Collectors.toList()));
        if (iCmsTeacherService.userAddRole(cmsUserRoleAdd)) {
            return CommonResult.success("添加成功");
        }
        return CommonResult.failed("添加失败");

    }

}
