package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacDormitory;

/**
 * <p>
 * 寝室   服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacDormitoryService extends IService<BacDormitory> {

}
