/*
 Navicat Premium Data Transfer

 Source Server         : 10.8.15.70
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : 10.8.15.70:33306
 Source Schema         : duty_system

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 25/11/2019 15:27:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bac_class
-- ----------------------------
DROP TABLE IF EXISTS `bac_class`;
CREATE TABLE `bac_class`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `major_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '专业编号',
  `college_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学院编号',
  `teacher_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '班主任id',
  `is_enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '行政班 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_college
-- ----------------------------
DROP TABLE IF EXISTS `bac_college`;
CREATE TABLE `bac_college`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门编号',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `parent_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '隶属部门',
  `sort` int(11) NULL DEFAULT 0 COMMENT '显示顺序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '院系档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_course
-- ----------------------------
DROP TABLE IF EXISTS `bac_course`;
CREATE TABLE `bac_course`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '课程代码',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '课程名称',
  `is_enabled` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程信息 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_dormitory
-- ----------------------------
DROP TABLE IF EXISTS `bac_dormitory`;
CREATE TABLE `bac_dormitory`  (
  `id` bigint(30) NOT NULL COMMENT 'ID',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `room` int(11) NOT NULL DEFAULT 100 COMMENT '房间号',
  `buildings` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '楼幢',
  `class_id` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '所属班级 可有多个班级用逗号隔开',
  `class_teacher_number` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '班主任 可有多个班主任用逗号隔开',
  `instructor_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '辅导员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '寝室  ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_major
-- ----------------------------
DROP TABLE IF EXISTS `bac_major`;
CREATE TABLE `bac_major`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '专业代码',
  `professional_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '专业名称',
  `director_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '专业主任',
  `college_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属学院',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '专业档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_stu_teach
-- ----------------------------
DROP TABLE IF EXISTS `bac_stu_teach`;
CREATE TABLE `bac_stu_teach`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `student_inumber` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '学生编号',
  `class_id` bigint(30) NULL DEFAULT NULL COMMENT '教学班编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生教学班关系表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_teaching_class
-- ----------------------------
DROP TABLE IF EXISTS `bac_teaching_class`;
CREATE TABLE `bac_teaching_class`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `teaching_class_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '教学班名称',
  `teacher_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '授课老师编号',
  `specialty_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '专业编号',
  `college_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学院编号',
  `is_enable` bit(1) NOT NULL DEFAULT b'1' COMMENT '启停状态',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教学班 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_term
-- ----------------------------
DROP TABLE IF EXISTS `bac_term`;
CREATE TABLE `bac_term`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `term_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '学期名称',
  `opening_date` datetime(0) NULL DEFAULT NULL COMMENT '开学日期',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '结束日期',
  `teaching_week` int(10) NULL DEFAULT 1 COMMENT '教学周次',
  `current_state` int(10) NOT NULL DEFAULT 0 COMMENT '当前状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学期信息 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_timetable
-- ----------------------------
DROP TABLE IF EXISTS `bac_timetable`;
CREATE TABLE `bac_timetable`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `course_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '课程代码',
  `course_title` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '课程名称',
  `teaching_class_id` bigint(30) NULL DEFAULT NULL COMMENT '教学班编号',
  `week_day` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '星期',
  `section` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '节次',
  `week` int(10) NULL DEFAULT 1 COMMENT '周次',
  `class_date` datetime(0) NULL DEFAULT NULL COMMENT '上课日期',
  `class_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上课时间',
  `teacher_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '教师编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课表档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_training_center
-- ----------------------------
DROP TABLE IF EXISTS `bac_training_center`;
CREATE TABLE `bac_training_center`  (
  `id` bigint(30) NOT NULL,
  `center_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '中心名称',
  `college_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '所属学院',
  `level` int(11) NOT NULL DEFAULT 0 COMMENT '级别',
  `type` int(11) NOT NULL DEFAULT 0 COMMENT '类型',
  `category` int(11) NOT NULL DEFAULT 0 COMMENT '类别',
  `design_date` date NULL DEFAULT NULL COMMENT '设计年月',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '地址',
  `director_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '实训中心主任编号',
  `honor` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '获得荣誉',
  `introduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '中心介绍',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实训中心  ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_training_room
-- ----------------------------
DROP TABLE IF EXISTS `bac_training_room`;
CREATE TABLE `bac_training_room`  (
  `id` bigint(30) NOT NULL COMMENT '实训室ID',
  `room_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '实训室名称',
  `trainingcenter_id` bigint(30) NULL DEFAULT NULL COMMENT '实训中心 关联实训中心表的实训中心id',
  `professional_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '服务专业',
  `courses` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '开设课程',
  `location` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '具体地点',
  `people_number` int(10) NULL DEFAULT 0 COMMENT '人数容量',
  `responsible_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理责任人编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实训室 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for biz_notice
-- ----------------------------
DROP TABLE IF EXISTS `biz_notice`;
CREATE TABLE `biz_notice`  (
  `id` bigint(30) NOT NULL COMMENT '编号',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `type` int(11) NULL DEFAULT 0 COMMENT '类型',
  `object_type` int(11) NULL DEFAULT 0 COMMENT '通知对象类型',
  `admin_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '管理员id',
  `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '业务消息表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for biz_notice_object
-- ----------------------------
DROP TABLE IF EXISTS `biz_notice_object`;
CREATE TABLE `biz_notice_object`  (
  `id` bigint(30) NOT NULL COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` int(11) NULL DEFAULT 0 COMMENT '逻辑删除',
  `notice_id` bigint(20) NULL DEFAULT NULL COMMENT '消息id',
  `user_type` int(11) NULL DEFAULT 0 COMMENT '用户类型',
  `user_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户id',
  `is_read` bit(1) NULL DEFAULT b'0' COMMENT '是否已读',
  `read_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '阅读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '业务消息对象表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_option_log
-- ----------------------------
DROP TABLE IF EXISTS `cms_option_log`;
CREATE TABLE `cms_option_log`  (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '业务名称',
  `log_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '日志类型',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求IP',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求资源路径',
  `req_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '请求方式',
  `req_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求参数',
  `res_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '返回参数',
  `exception` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '异常信息',
  `timeout` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '耗时',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户唯一编号',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '访问日志信息 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_permission
-- ----------------------------
DROP TABLE IF EXISTS `cms_permission`;
CREATE TABLE `cms_permission`  (
  `id` bigint(20) NOT NULL COMMENT '权限ID 权限ID',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间 创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间 更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否禁用 0->可用，1->禁用',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '父级权限ID 父级权限ID',
  `permission_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限名称 权限中文名称',
  `value` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限编码 权限编码',
  `type` int(11) NOT NULL DEFAULT 0 COMMENT '权限类型 权限类型：0->目录；1->菜单；2->按钮（接口绑定权限）',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '前端路由路径 前端路由路径',
  `meta` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '元数据',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '路由名称',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '组件路由',
  `redirect` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '重定向路由',
  `ban` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否被禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'cms_permission ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cms_role
-- ----------------------------
DROP TABLE IF EXISTS `cms_role`;
CREATE TABLE `cms_role`  (
  `id` bigint(30) NOT NULL COMMENT '角色ID 角色ID',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间 创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间 更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除 0->未删除，1->删除',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '角色中文名称 角色中文名称',
  `auth_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '权限码 权限编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'cms_role ' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cms_student
-- ----------------------------
DROP TABLE IF EXISTS `cms_student`;
CREATE TABLE `cms_student`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '登录密码',
  `administrtive_classid` bigint(30) NULL DEFAULT NULL COMMENT '行政班编号',
  `student_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '学号',
  `bed` int(11) NULL DEFAULT NULL COMMENT '床号',
  `specialty_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '专业编号',
  `status` int(11) NULL DEFAULT 0 COMMENT '学籍状态',
  `college_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '所属学院',
  `head_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '班主任(教师)编号',
  `is_enable` bit(1) NULL DEFAULT b'1' COMMENT '启停状态',
  `is_graduate` bit(1) NULL DEFAULT b'0' COMMENT '是否毕业',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_teacher
-- ----------------------------
DROP TABLE IF EXISTS `cms_teacher`;
CREATE TABLE `cms_teacher`  (
  `id` bigint(30) NOT NULL COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '职工号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录密码',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '姓名',
  `sex` int(11) NULL DEFAULT 0 COMMENT '性别',
  `college_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '所属部门 根据这个部门查所属学院',
  `type` int(11) NULL DEFAULT NULL COMMENT '教师类型',
  `is_enabled` bit(1) NULL DEFAULT NULL COMMENT '启停状态',
  `auth_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL COMMENT '权限集合',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci NULL DEFAULT '' COMMENT '头像地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教师档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_inspect_child
-- ----------------------------
DROP TABLE IF EXISTS `ise_inspect_child`;
CREATE TABLE `ise_inspect_child`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `item_id` bigint(30) NULL DEFAULT NULL COMMENT '检查项id',
  `inspection_id` bigint(30) NULL DEFAULT NULL COMMENT '检查总表id',
  `object_id` bigint(30) NULL DEFAULT NULL COMMENT '检查对象id',
  `result` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '检查结果',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '检查子表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_inspect_items
-- ----------------------------
DROP TABLE IF EXISTS `ise_inspect_items`;
CREATE TABLE `ise_inspect_items`  (
  `id` bigint(30) NOT NULL COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '检查项目名',
  `type` int(11) NULL DEFAULT 0 COMMENT '检查类型',
  `describtion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '描述',
  `is_enable` bit(1) NULL DEFAULT b'1' COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '检查项目表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_inspection
-- ----------------------------
DROP TABLE IF EXISTS `ise_inspection`;
CREATE TABLE `ise_inspection`  (
  `id` bigint(30) NOT NULL COMMENT '编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `duty_date` date NULL DEFAULT NULL COMMENT '值班日期',
  `week` int(10) NOT NULL DEFAULT 1 COMMENT '周次',
  `duty_teacher` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '值班人员',
  `check_teacher` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '检查人员',
  `check_type` int(11) NOT NULL DEFAULT 0 COMMENT '检查类型',
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `requires` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '整改要求 检查结果及存在问题',
  `result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '整改结果 问题反馈及解决情况',
  `item_check_id` bigint(30) NULL DEFAULT NULL COMMENT '检查对象编号 关联检查对象的id，比如寝室检查关联寝室id',
  `feedback_status` int(10) NULL DEFAULT 0 COMMENT '反馈状态 表示是/否反馈和已反馈/待反馈',
  `teachers_numbers` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '其他相关老师 单个或多个老师',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_schedule
-- ----------------------------
DROP TABLE IF EXISTS `ise_schedule`;
CREATE TABLE `ise_schedule`  (
  `id` bigint(30) NOT NULL COMMENT '编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `term_id` bigint(30) NULL DEFAULT NULL COMMENT '学期id',
  `week` int(10) NULL DEFAULT 0 COMMENT '周次',
  `leader_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '值班领导id',
  `middle_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '值班中层id',
  `duty_one_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '值班班主任id',
  `duty_two_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '值班班主任2id',
  `instructor_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '值班辅导员',
  `creater_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建人id',
  `plan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '计划',
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '总结',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '值班计划  ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_student_inspect
-- ----------------------------
DROP TABLE IF EXISTS `ise_student_inspect`;
CREATE TABLE `ise_student_inspect`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `item_id` bigint(30) NULL DEFAULT NULL COMMENT '检查项id',
  `inspection_id` bigint(30) NULL DEFAULT NULL COMMENT '检查总表id',
  `student_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '学生id',
  `result` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '检查结果',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生检查表 ' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
