package com.jhc.entity;

import com.jhc.dto.CmsTreePermission;
import lombok.Data;

import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/15 14:59
 */
@Data
public class RedisUserInfo {
    /**
     * token
     */
    private String token;

    /**
     * 权限列表
     */
    private List<CmsPermission> permissionList;

    /**
     * 路由
     */
    private List<CmsTreePermission> router;
}
