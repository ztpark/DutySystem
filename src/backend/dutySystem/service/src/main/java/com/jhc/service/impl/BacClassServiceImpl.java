package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacClass;
import com.jhc.mapper.BacClassMapper;
import com.jhc.service.IBacClassService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 行政班  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacClassServiceImpl extends ServiceImpl<BacClassMapper, BacClass> implements IBacClassService {

}
