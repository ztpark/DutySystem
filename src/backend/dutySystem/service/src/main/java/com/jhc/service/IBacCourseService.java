package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacCourse;

/**
 * <p>
 * 课程信息  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacCourseService extends IService<BacCourse> {

}
