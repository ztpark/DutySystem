package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacTerm;
import com.jhc.mapper.BacTermMapper;
import com.jhc.service.IBacTermService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学期信息  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTermServiceImpl extends ServiceImpl<BacTermMapper, BacTerm> implements IBacTermService {

}
