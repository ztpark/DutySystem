package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacCourse;
import com.jhc.mapper.BacCourseMapper;
import com.jhc.service.IBacCourseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程信息  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacCourseServiceImpl extends ServiceImpl<BacCourseMapper, BacCourse> implements IBacCourseService {

}
