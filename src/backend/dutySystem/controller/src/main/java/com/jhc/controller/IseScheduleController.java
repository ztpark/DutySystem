package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 值班计划   前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/zfm/ise-schedule")
public class IseScheduleController {

}
