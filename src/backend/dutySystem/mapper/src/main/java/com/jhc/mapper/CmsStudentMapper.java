package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsStudent;

/**
 * <p>
 * 学生  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface CmsStudentMapper extends BaseMapper<CmsStudent> {

}
