package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacMajor;

/**
 * <p>
 * 专业档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacMajorMapper extends BaseMapper<BacMajor> {

}
