package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 业务消息对象表 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BizNoticeObject extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息id
     */
    private Long noticeId;

    /**
     * 用户类型
     */
    private Integer userType;

    /**
     * 用户id
     */
    private String userNumber;

    /**
     * 是否已读
     */
    private Boolean isRead;

    /**
     * 阅读时间
     */
    private LocalDateTime readTime;


}
