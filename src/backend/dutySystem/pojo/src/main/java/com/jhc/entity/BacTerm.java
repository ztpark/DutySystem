package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 学期信息 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacTerm extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学期名称
     */
    private String termName;

    /**
     * 开学日期
     */
    private LocalDateTime openingDate;

    /**
     * 结束日期
     */
    private LocalDateTime endDate;

    /**
     * 教学周次
     */
    private Integer teachingWeek;

    /**
     * 当前状态
     */
    private Integer currentState;


}
