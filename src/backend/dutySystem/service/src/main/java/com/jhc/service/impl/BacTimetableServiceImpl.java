package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacTimetable;
import com.jhc.mapper.BacTimetableMapper;
import com.jhc.service.IBacTimetableService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课表档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTimetableServiceImpl extends ServiceImpl<BacTimetableMapper, BacTimetable> implements IBacTimetableService {

}
