package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacTimetable;

/**
 * <p>
 * 课表档案  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTimetableService extends IService<BacTimetable> {

}
