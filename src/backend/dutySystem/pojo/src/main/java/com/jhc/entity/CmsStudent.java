package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 学生 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CmsStudent extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 行政班编号
     */
    private Long administrtiveClassid;

    /**
     * 学号
     */
    private String studentNumber;

    /**
     * 床号
     */
    private Integer bed;

    /**
     * 专业编号
     */
    private String specialtyNumber;

    /**
     * 学籍状态
     */
    private Integer status;

    /**
     * 所属学院
     */
    private String collegeNumber;

    /**
     * 班主任(教师)编号
     */
    private String headNumber;

    /**
     * 启停状态
     */
    private Boolean isEnable;

    /**
     * 是否毕业
     */
    private Boolean isGraduate;


}
