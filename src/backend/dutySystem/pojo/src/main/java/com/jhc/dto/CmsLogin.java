package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: zfm
 * @Date: 2019/11/13 14:57
 */
@Data
public class CmsLogin {
    @NotBlank(message = "工号不能为空")
    private String staffNumber;
    @NotBlank(message = "密码不能为空")
    private String password;
}
