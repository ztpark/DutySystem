package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/15 09:49
 */
@Data
public class CmsTreePermission {

    /**
     * 用户ID
     */
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 父id
     */
    private Long parentId;

    /**
     * 权限值
     */
    private String value;

    /**
     * 权限名称
     */
    private String permissionName;

    /**
     * 元数据
     */
    private String meta;

    /**
     * 前端路由路径 前端路由路径
     */
    private String path;

    /**
     * 路由名称
     */
    private String name;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 重定向路径
     */
    private String redirect;

    /**
     * 子权限
     */
    private List<CmsTreePermission> childrenList;
}
