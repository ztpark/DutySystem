package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacStuTeach;

/**
 * <p>
 * 学生教学班关系表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacStuTeachService extends IService<BacStuTeach> {

}
