package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;

import java.util.List;

/**
 * <p>
 * cms_permission  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface ICmsPermissionService extends IService<CmsPermission> {

    /**
     * 恢复权限使用
     *
     * @param permissionId 权限ID
     * @return 是否恢复
     */
    Boolean restart(List<Long> permissionId);


    /**
     * 禁用权限使用
     *
     * @param permissionId 权限ID
     * @return 是否禁用
     */
    Boolean ban(List<Long> permissionId);

    /**
     * 查询所有权限
     * @return 树型结构
     */
    List<CmsTreePermission> tree();

}
