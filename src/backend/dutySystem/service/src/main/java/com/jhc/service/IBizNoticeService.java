package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BizNotice;

/**
 * <p>
 * 业务消息表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBizNoticeService extends IService<BizNotice> {

}
