package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacTeachingClass;

/**
 * <p>
 * 教学班  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTeachingClassMapper extends BaseMapper<BacTeachingClass> {

}
