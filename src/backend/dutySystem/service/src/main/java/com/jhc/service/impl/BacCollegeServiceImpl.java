package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacCollege;
import com.jhc.mapper.BacCollegeMapper;
import com.jhc.service.IBacCollegeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 院系档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacCollegeServiceImpl extends ServiceImpl<BacCollegeMapper, BacCollege> implements IBacCollegeService {

}
