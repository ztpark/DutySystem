package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacDormitory;
import com.jhc.mapper.BacDormitoryMapper;
import com.jhc.service.IBacDormitoryService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 寝室   服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacDormitoryServiceImpl extends ServiceImpl<BacDormitoryMapper, BacDormitory> implements IBacDormitoryService {

}
