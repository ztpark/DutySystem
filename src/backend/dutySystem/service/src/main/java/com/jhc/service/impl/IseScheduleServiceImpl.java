package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.IseSchedule;
import com.jhc.mapper.IseScheduleMapper;
import com.jhc.service.IIseScheduleService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 值班计划   服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class IseScheduleServiceImpl extends ServiceImpl<IseScheduleMapper, IseSchedule> implements IIseScheduleService {

}
