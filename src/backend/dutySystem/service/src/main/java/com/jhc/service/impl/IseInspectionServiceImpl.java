package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.IseInspection;
import com.jhc.mapper.IseInspectionMapper;
import com.jhc.service.IIseInspectionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class IseInspectionServiceImpl extends ServiceImpl<IseInspectionMapper, IseInspection> implements IIseInspectionService {

}
