package com.jhc.bo;

import com.jhc.entity.CmsPermission;
import com.jhc.entity.CmsTeacher;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: zfm
 * @Date: 2019/11/11 22:45
 */
public class AdminUserDetails implements UserDetails {

    private CmsTeacher cmsTeacher;
    private List<CmsPermission> permissionList;

    public AdminUserDetails(CmsTeacher cmsTeacher, List<CmsPermission> permissionList) {
        this.cmsTeacher = cmsTeacher;
        this.permissionList = permissionList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 返回当前用户的权限
        return permissionList.stream()
                .filter(permission -> permission.getValue() != null)
                .map(permission -> new SimpleGrantedAuthority(permission.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return cmsTeacher.getPassword();
    }

    @Override
    public String getUsername() {
        return cmsTeacher.getNumber();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return cmsTeacher.getIsEnabled();
    }
}
