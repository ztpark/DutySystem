package com.jhc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 教学班 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacTeachingClass extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 序号
     */
    @TableId(value = "class_id", type = IdType.AUTO)
    private Long classId;

    /**
     * 教学班名称
     */
    private String teachingClassName;

    /**
     * 授课老师编号
     */
    private String teacherNumber;

    /**
     * 专业编号
     */
    private String specialtyNumber;

    /**
     * 学院编号
     */
    private String collegeNumber;

    /**
     * 启停状态
     */
    private Boolean isEnable;


}
