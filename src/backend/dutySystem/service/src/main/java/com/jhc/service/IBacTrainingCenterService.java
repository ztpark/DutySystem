package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacTrainingCenter;

/**
 * <p>
 * 实训中心   服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTrainingCenterService extends IService<BacTrainingCenter> {

}
