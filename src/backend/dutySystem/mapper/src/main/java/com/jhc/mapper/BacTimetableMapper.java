package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacTimetable;

/**
 * <p>
 * 课表档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTimetableMapper extends BaseMapper<BacTimetable> {

}
