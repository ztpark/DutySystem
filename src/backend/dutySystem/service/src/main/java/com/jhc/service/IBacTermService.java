package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacTerm;

/**
 * <p>
 * 学期信息  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTermService extends IService<BacTerm> {

}
