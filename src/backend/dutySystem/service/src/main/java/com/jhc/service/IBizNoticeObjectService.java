package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BizNoticeObject;

/**
 * <p>
 * 业务消息对象表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBizNoticeObjectService extends IService<BizNoticeObject> {

}
