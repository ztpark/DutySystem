package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/14 15:32
 */
@Data
public class CmsUserPermissionAdd {

    @NotNull(message = "用户ID不能为空")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @NotNull(message = "权限码不能为空")
    private List<String> authList;

    /**
     * 是否马上生效
     */
    private Boolean ifFast = false;
}
