package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.IseSchedule;

/**
 * <p>
 * 值班计划   服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IIseScheduleService extends IService<IseSchedule> {

}
