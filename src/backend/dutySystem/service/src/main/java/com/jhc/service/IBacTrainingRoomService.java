package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacTrainingRoom;

/**
 * <p>
 * 实训室  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTrainingRoomService extends IService<BacTrainingRoom> {

}
