package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IseInspection extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 值班日期
     */
    private LocalDate dutyDate;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 值班人员
     */
    private String dutyTeacher;

    /**
     * 检查人员
     */
    private String checkTeacher;

    /**
     * 检查类型
     */
    private Integer checkType;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    private String requires;

    /**
     * 整改结果 问题反馈及解决情况
     */
    private String result;

    /**
     * 检查对象编号 关联检查对象的id，比如寝室检查关联寝室id
     */
    private Long itemCheckId;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    private Integer feedbackStatus;

    /**
     * 其他相关老师 单个或多个老师
     */
    private String teachersNumbers;


}
