package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BizNoticeObject;

/**
 * <p>
 * 业务消息对象表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BizNoticeObjectMapper extends BaseMapper<BizNoticeObject> {

}
