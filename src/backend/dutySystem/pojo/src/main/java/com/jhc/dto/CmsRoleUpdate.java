package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/13 20:01
 */
@Data
public class CmsRoleUpdate {

    @NotNull(message = "角色ID不能为空")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 角色中文名称 角色中文名称
     */
    @NotBlank(message = "角色名称不能为空")
    private String name;

    /**
     * 权限码 权限编码
     */
    @NotNull(message = "权限码不能为空")
    private List<String> authDataList;

    /**
     * 是否马上让用户下线
     */
    private Boolean ifFast = false;
}
