package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacTeachingClass;
import com.jhc.mapper.BacTeachingClassMapper;
import com.jhc.service.IBacTeachingClassService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 教学班  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTeachingClassServiceImpl extends ServiceImpl<BacTeachingClassMapper, BacTeachingClass> implements IBacTeachingClassService {

}
