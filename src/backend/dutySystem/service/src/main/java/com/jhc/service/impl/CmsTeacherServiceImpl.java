package com.jhc.service.impl;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.CmsRegister;
import com.jhc.dto.CmsTreePermission;
import com.jhc.dto.CmsUserPermissionAdd;
import com.jhc.dto.CmsUserRoleAdd;
import com.jhc.entity.*;
import com.jhc.mapper.CmsPermissionMapper;
import com.jhc.mapper.CmsRoleMapper;
import com.jhc.mapper.CmsTeacherMapper;
import com.jhc.service.ICmsTeacherService;
import com.jhc.service.RedisService;
import com.jhc.utils.JHC;
import com.jhc.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 教师档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
@Slf4j
public class CmsTeacherServiceImpl extends ServiceImpl<CmsTeacherMapper, CmsTeacher> implements ICmsTeacherService {

    @Autowired
    private CmsTeacherMapper cmsTeacherMapper;

    @Autowired
    private CmsRoleMapper cmsRoleMapper;

    @Autowired
    private CmsPermissionMapper cmsPermissionMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RedisService redisService;

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Value("${jwt.expiration}")
    private Long redisTime;

    /**
     * 获取用户所有权限
     *
     * @param staffNumber 用户账号 职工号
     * @return 用户权限列表
     */
    @Override
    public List<CmsPermission> getPermissionList(String staffNumber) {

        QueryWrapper<CmsTeacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("auth_data").lambda().eq(CmsTeacher::getNumber, staffNumber);
        CmsTeacher cmsAdmin = cmsTeacherMapper.selectOne(queryWrapper);
        if (cmsAdmin == null) {
            return new ArrayList<>();
        }
        try {
            UserAuthData userAuthData = JSONUtil.toBean(cmsAdmin.getAuthData(), UserAuthData.class);
            List<String> authList = userAuthData.getAuthList();
            List<String> roleList = userAuthData.getRoleList();
            QueryWrapper<CmsRole> roleQueryWrapper = new QueryWrapper<>();
            roleQueryWrapper.select("auth_data").lambda().in(CmsRole::getId, roleList);
            List<CmsRole> authRoleList = cmsRoleMapper.selectList(roleQueryWrapper);
            for (CmsRole cmsRole : authRoleList) {
                UserAuthData roleAuth = JSONUtil.toBean(cmsRole.getAuthData(), UserAuthData.class);
                if (roleAuth.getAuthList() != null) {
                    authList.addAll(roleAuth.getAuthList());
                }
            }
            authList = authList.stream().distinct().collect(Collectors.toList());
            List<CmsPermission> cmsPermissionList = cmsPermissionMapper.selectList(
                    new QueryWrapper<CmsPermission>().lambda().in(CmsPermission::getValue, authList).eq(CmsPermission::getBan, false)
            );

            return cmsPermissionList;
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return new ArrayList<>();
    }

    /**
     * 用户注册
     *
     * @param cmsRegister 注册实体类
     * @return 是否注册成功
     */
    @Override
    public Boolean register(CmsRegister cmsRegister) {
        CmsTeacher cmsTeacher = new CmsTeacher();
        BeanUtils.copyProperties(cmsRegister, cmsTeacher);
        // 保留json对象格式
        cmsTeacher.setAuthData("{}");
        // 查询是否有相通用户名
        if (cmsTeacherMapper.selectOne(new QueryWrapper<CmsTeacher>().lambda()
                .eq(CmsTeacher::getNumber, cmsRegister.getNumber())) != null) {
            return false;
        }
        // 密码加密
        String encodePassword = passwordEncoder.encode(cmsTeacher.getPassword());
        cmsTeacher.setPassword(encodePassword);
        return cmsTeacherMapper.insert(cmsTeacher) == 1;
    }

    /**
     * 用户登陆
     *
     * @param username 用户名
     * @param password 密码
     * @return token
     */
    @Override
    public RedisUserInfo login(String username, String password) {
        String token = null;
        RedisUserInfo redisUserInfo = new RedisUserInfo();
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (!passwordEncoder.matches(password, userDetails.getPassword())) {
                throw new BadCredentialsException("密码不正确");
            }
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities()
            );
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            token = jwtTokenUtil.generateToken(userDetails);

            // 获取用户所携带的路由信息
            List<CmsPermission> cmsPermissionList = getPermissionList(username);
            List<CmsTreePermission> cmsTreePermissionList = JHC.cmsTreePermissions(cmsPermissionList);

            redisUserInfo.setPermissionList(cmsPermissionList);
            redisUserInfo.setRouter(cmsTreePermissionList);
            redisUserInfo.setToken(tokenHead + token);
            redisService.set(username, JSON.toJSONString(redisUserInfo));
            redisService.expire(username, redisTime);

        } catch (Exception e) {
            log.warn("登陆异常:{}", e.getMessage());
        }
        return redisUserInfo;
    }

    /**
     * 给用户添加权限
     *
     * @param cmsUserPermissionAdd 添加权限对象
     * @return 是否添加成功
     */
    @Override
    public Boolean userAddPermission(CmsUserPermissionAdd cmsUserPermissionAdd) {
        CmsTeacher cmsTeacher = getUserAuth(cmsUserPermissionAdd.getId());
        if (cmsTeacher == null) {
            return false;
        }
        // 拿出原本的角色 + 权限 字符串
        String authStr = cmsTeacher.getAuthData();
        // 把字符串转对象
        UserAuthData userAuthData = JSONUtil.toBean(authStr, UserAuthData.class);
        // 把新的权限列表设置到对象中
        userAuthData.setAuthList(cmsUserPermissionAdd.getAuthList());
        // 把对象转化成json设置到对象中
        cmsTeacher.setAuthData(JSONUtil.toJsonStr(userAuthData));
        // 清除token
        if (cmsTeacherMapper.updateById(cmsTeacher) == 1) {
            clearToken(cmsUserPermissionAdd.getIfFast(), cmsUserPermissionAdd.getId());
            return true;
        }
        return false;
    }

    /**
     * 给用户添加角色
     *
     * @param cmsUserRoleAdd 添加角色对象
     * @return 是否添加成功
     */
    @Override
    public Boolean userAddRole(CmsUserRoleAdd cmsUserRoleAdd) {
        CmsTeacher cmsTeacher = getUserAuth(cmsUserRoleAdd.getId());
        if (cmsTeacher == null) {
            return false;
        }
        // 拿出原本的角色 + 权限 字符串
        String authStr = cmsTeacher.getAuthData();
        // 把字符串转化成对象（原本的）
        UserAuthData userAuthData = JSONUtil.toBean(authStr, UserAuthData.class);
        // 把新的角色信息放进去
        userAuthData.setRoleList(cmsUserRoleAdd.getRoleList());
        // 把对象转化成json存到对象
        cmsTeacher.setAuthData(JSONUtil.toJsonStr(userAuthData));
        // 清除token
        if (cmsTeacherMapper.updateById(cmsTeacher) == 1) {
            clearToken(cmsUserRoleAdd.getIfFast(), cmsUserRoleAdd.getId());
            return true;
        }
        return false;
    }

    private void clearToken(boolean ifFast, Long id) {
        // 判断是否马上登出
        CmsTeacher cmsTeacher = cmsTeacherMapper.selectOne(new QueryWrapper<CmsTeacher>()
                .lambda()
                .select(CmsTeacher::getNumber)
                .eq(CmsTeacher::getId, id));
        if (ifFast && cmsTeacher != null) {
            redisService.remove(cmsTeacher.getNumber());
        } else if (!ifFast && cmsTeacher != null) {
            redisService.expire(cmsTeacher.getNumber(), 60 * 60);
        }
    }

    /**
     * 根据ID查找用户id和身份信息
     *
     * @param id 用户ID
     * @return 带有id和身份信息的实体对象
     */
    private CmsTeacher getUserAuth(Long id) {
        LambdaQueryWrapper<CmsTeacher> queryWrapper = new QueryWrapper<CmsTeacher>()
                .select("auth_data", "id").lambda()
                .eq(CmsTeacher::getId, id);
        return cmsTeacherMapper.selectOne(queryWrapper);
    }

}
