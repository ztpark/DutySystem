package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 专业档案 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacMajor extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 专业代码
     */
    private String number;

    /**
     * 专业名称
     */
    private String professionalName;

    /**
     * 专业主任
     */
    private String directorNumber;

    /**
     * 所属学院
     */
    private String collegeNumber;


}
