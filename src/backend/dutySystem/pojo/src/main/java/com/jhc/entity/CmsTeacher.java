package com.jhc.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 教师档案 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CmsTeacher extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 职工号
     */
    private String number;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 所属部门 根据这个部门查所属学院
     */
    private String collegeNumber;

    /**
     * 教师类型
     */
    private Integer type;

    /**
     * 启停状态
     */
    private Boolean isEnabled;

    /**
     * 权限值
     */
    private String authData;


}
