package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BizNotice;
import com.jhc.mapper.BizNoticeMapper;
import com.jhc.service.IBizNoticeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务消息表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BizNoticeServiceImpl extends ServiceImpl<BizNoticeMapper, BizNotice> implements IBizNoticeService {

}
