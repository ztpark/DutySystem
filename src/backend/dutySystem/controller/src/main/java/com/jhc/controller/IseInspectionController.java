package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/zfm/ise-inspection")
public class IseInspectionController {

}
