package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacTeachingClass;

/**
 * <p>
 * 教学班  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTeachingClassService extends IService<BacTeachingClass> {

}
