package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacTrainingRoom;

/**
 * <p>
 * 实训室  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTrainingRoomMapper extends BaseMapper<BacTrainingRoom> {

}
